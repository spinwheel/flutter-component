@echo off

echo ________________________________      DELETE PUBSPEC.LOCK COMPONENT
call del "pubspec.lock"
echo ________________________________      FLUTTER CLEAN COMPONENT
call flutter clean
echo ________________________________      FLUTTER PUB UPGRADE COMPONENT
call flutter pub upgrade
echo ________________________________      FLUTTER PUB GET COMPONENT
call flutter pub get