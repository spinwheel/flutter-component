import 'package:flutter/material.dart';

class CardRun extends StatelessWidget {
  const CardRun(
    this.text,
    this.onTap, {
    Key? key,
  }) : super(key: key);

  final String text;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.6,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Icon(Icons.play_arrow, color: Colors.blue),
            ],
          ),
        ),
        decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: const BorderRadius.all(Radius.circular(5)),
            border: Border.all(color: Colors.blue, width: 1, style: BorderStyle.solid)),
      ),
    );
  }
}
