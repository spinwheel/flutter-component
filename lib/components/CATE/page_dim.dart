import 'package:flutter/material.dart';

class PageDim extends StatelessWidget {
  const PageDim(
    this.children, {
    Key? key,
  }) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.vertical,
      children: [
        Expanded(
          child: ListView(
            children: children,
          ),
        )
      ],
    );
  }
}
