import 'package:flutter/material.dart';

class PageRun extends StatelessWidget {
  const PageRun(
    this.children, {
    Key? key,
  }) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: 2,
              crossAxisSpacing: 15,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              mainAxisSpacing: 10,
              scrollDirection: Axis.vertical,
              children: children,
            ),
          )
        ],
      ),
    );
  }
}
