import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_core/navigation/nav.dart';

class ExitHeader extends StatelessWidget {
  const ExitHeader({
    Function()? onExit,
    Key? key,
  })  : _onExit = onExit,
        super(key: key);

  final Function()? _onExit;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: 30,
        child: Align(
          alignment: Alignment.centerRight,
          child: GestureDetector(
            onTap: _onExit ?? () => back(context),
            child: const ImageCompat(
              'exit_icon.svg',
              package: 'sw_loan_connect',
              width: 25,
              height: 25,
            ),
          ),
        ));
  }
}
