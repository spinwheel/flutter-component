import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/sw_pie_chart.dart';

import 'box_card.dart';

class PrecisionPayCard extends StatelessWidget {
  const PrecisionPayCard({
    required double chartValue,
    required String cardTitle,
    required String currentValue,
    required String wSpinwheelValue,
    required String savingsValue,
    EdgeInsets? padding,
    EdgeInsets? margin,
    Key? key,
  })  : _chartValue = chartValue,
        _cardTitle = cardTitle,
        _currentValue = currentValue,
        _wSpinwheelValue = wSpinwheelValue,
        _savingsValue = savingsValue,
        _margin = margin,
        _padding = padding,
        super(key: key);

  final double _chartValue;
  final String _cardTitle;
  final String _currentValue;
  final String _wSpinwheelValue;
  final String _savingsValue;
  final EdgeInsets? _padding;
  final EdgeInsets? _margin;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 140,
      child: BoxCard(
        margin: _margin,
        padding: _padding,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _title(_cardTitle),
            const SizedBox(
              width: 10,
            ),
            SWPieChart(
              value: _chartValue,
            ),
          ],
        ),
      ),
    );
  }

  Widget _title(String title) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(_cardTitle),
        Row(
          children: [
            _current(),
            const SizedBox(width: 20),
            _wSpinwheel(),
            const SizedBox(width: 20),
            _savings(),
          ],
        ),
      ],
    );
  }

  _current() => _newColumn(
        const Text(
          "Current",
          style: TextStyle(color: Color(0xFF5F5F5F)),
        ),
        Text("\$$_currentValue"),
      );

  _wSpinwheel() => _newColumn(
        const Text(
          "W/Spinwheel",
          style: TextStyle(color: Color(0xFF5F5F5F)),
        ),
        Text("\$$_wSpinwheelValue"),
      );

  _savings() => _newColumn(
        const Text(
          "Savings",
          style: TextStyle(color: Color(0xFF5F5F5F)),
        ),
        Text("\$$_savingsValue"),
      );

  Widget _newColumn(Widget title, Widget value) => Column(
        children: [
          title,
          const SizedBox(
            height: 8,
          ),
          value
        ],
      );
}
