import 'package:flutter/material.dart';
import 'package:sw_component/tool/ext_state.dart';

import 'box_card.dart';
import 'image_compat.dart';

class HistoryCard extends StatefulWidget {
  const HistoryCard({
    this.image,
    this.index = '-',
    this.date = '--- -- ----',
    this.value = '\$ ---',
    this.detail0 = '--',
    this.detail1 = '--',
    this.onPressed,
    this.textColor,
    this.backgroundColor,
    this.textStyle,
    Key? key,
  }) : super(key: key);

  const HistoryCard.pos([
    this.image,
    this.index = '-',
    this.date = '-- / -- / ----',
    this.value = '\$--',
    this.detail0 = '--',
    this.detail1 = '--',
    this.onPressed,
    this.textColor,
    this.backgroundColor,
    this.textStyle,
    Key? key,
  ]) : super(key: key);

  final String index;
  final String date;
  final String value;
  final String detail0;
  final String detail1;
  final Widget? image;
  final void Function()? onPressed;
  final Color? textColor;
  final Color? backgroundColor;
  final TextStyle? textStyle;

  @override
  State<HistoryCard> createState() => _HistoryCardState();
}

class _HistoryCardState extends State<HistoryCard> {
  @override
  build(BuildContext context) => TextButton(
        onPressed: widget.onPressed,
        child: BoxCard(
          margin: const EdgeInsets.only(bottom: 6),
          child: _buildContent(),
        ),
      );

  _buildContent() {
    final _defaultTextStyle = TextStyle(fontSize: 9, color: colorScheme.onSurface);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.index,
          textAlign: TextAlign.left,
          style: widget.textStyle ?? _defaultTextStyle,
        ),
        Flexible(
          flex: 1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text(
                    widget.date.substring(0, 6),
                    textAlign: TextAlign.left,
                    style: widget.textStyle ?? _defaultTextStyle,
                  ),
                  Text(
                    widget.date.substring(7, widget.date.length),
                    textAlign: TextAlign.left,
                    style: widget.textStyle ?? _defaultTextStyle,
                  ),
                ],
              ),
              Text(
                widget.value,
                textAlign: TextAlign.left,
                style: widget.textStyle ?? _defaultTextStyle,
              ),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.image ??
                      const ImageCompat(
                        'logo_spinwheel.png',
                        width: 48,
                        height: 48,
                      ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(
                    widget.detail0,
                    textAlign: TextAlign.left,
                    style: widget.textStyle ?? _defaultTextStyle,
                  ),
                ],
              ),
              const SizedBox(
                width: 5,
              ),
              Flexible(
                child: Text(
                  widget.detail1,
                  textAlign: TextAlign.right,
                  style: widget.textStyle ?? _defaultTextStyle,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
