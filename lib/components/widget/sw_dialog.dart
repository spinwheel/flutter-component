import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

class SWDialog extends StatelessWidget {
  const SWDialog(
    this._onLeftButtonPressed,
    this._onRightButtonPressed, {
    String dialogTitle = "Authentication successful.",
    String leftButtonText = "Add another account",
    String rightButtonText = "Continue",
    Key? key,
  })  : _dialogTitle = dialogTitle,
        _leftButtonText = leftButtonText,
        _rightButtonText = rightButtonText,
        super(key: key);

  final String _dialogTitle;
  final String _leftButtonText;
  final String _rightButtonText;
  final void Function() _onLeftButtonPressed;
  final void Function() _onRightButtonPressed;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.all(8),
        height: 150,
        width: MediaQuery.of(context).size.width,
        decoration: _dialogDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ..._dialogHeader(_dialogTitle),
            _dialogButtons(
              _leftButtonText,
              _rightButtonText,
              _onLeftButtonPressed,
              _onRightButtonPressed,
            ),
          ],
        ),
      ),
    );
  }

  BoxDecoration _dialogDecoration() => const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            spreadRadius: 1,
            blurRadius: 10,
            offset: Offset(6.0, 6.0),
          )
        ],
      );

  List<Widget> _dialogHeader(String dialogTitle) => [
        Text(
          dialogTitle,
          style: theme.textTheme.headline5,
        ),
        const Divider(
          color: Colors.black26,
          height: 2,
        )
      ];

  Widget _dialogButtons(
    String leftButtonText,
    String rightButtonText,
    void Function() onLeftButtonPressed,
    void Function() onRightButtonPressed,
  ) =>
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ElevatedButton(
            onPressed: onLeftButtonPressed,
            style: ElevatedButton.styleFrom(
              primary: colorScheme.primary,
              padding: const EdgeInsets.symmetric(horizontal: 45.0, vertical: 12.0),
            ),
            child: Text(
              leftButtonText,
              style: theme.textTheme.button,
            ),
          ),
          ElevatedButton(
            onPressed: onRightButtonPressed,
            style: ElevatedButton.styleFrom(
              primary: colorScheme.secondary,
              padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 12.0),
            ),
            child: Text(
              rightButtonText,
              style: theme.textTheme.button,
            ),
          ),
        ],
      );
}
