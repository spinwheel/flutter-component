import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

class SWOutlinedButton extends StatelessWidget {
  const SWOutlinedButton(
    this.text,
    this.onPressed, {
    double? width,
    double? height,
    Key? key,
  })  : _width = width,
        _height = height,
        super(key: key);

  final String text;
  final Function() onPressed;
  final double? _width;
  final double? _height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width ?? MediaQuery.of(context).size.width,
      height: _height,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          padding: const EdgeInsets.all(8),
          elevation: 0,
          shadowColor: Colors.transparent,
          primary: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4),
            side: BorderSide(
              width: 1,
              color: theme.colorScheme.primary,
            ),
          ),
        ),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: theme.colorScheme.primary,
          ),
        ),
      ),
    );
  }
}
