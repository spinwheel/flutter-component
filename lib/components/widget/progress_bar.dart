import 'package:flutter/material.dart';

class ProgressBar extends StatefulWidget {
  const ProgressBar({
    required this.value,
    this.color,
    this.backgroundColor,
    Key? key,
  }) : super(key: key);

  final double value;
  final Color? color;
  final Color? backgroundColor;

  @override
  _ProgressBarState createState() => _ProgressBarState();
}

class _ProgressBarState extends State<ProgressBar> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: LinearProgressIndicator(
        value: 222,
        color: widget.color ?? Theme.of(context).colorScheme.onBackground,
        backgroundColor: widget.backgroundColor ?? Theme.of(context).colorScheme.background,
      ),
    );
  }
}
