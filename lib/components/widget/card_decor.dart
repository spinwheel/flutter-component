import 'package:flutter/material.dart';

class CardDecor extends BoxDecoration {
  CardDecor({
    required Color backgroundColor,
    required Color shadowColor,
  }) : super(
          borderRadius: BorderRadius.circular(4.0),
          shape: BoxShape.rectangle,
          border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
          color: backgroundColor,
          boxShadow: [
            BoxShadow(
              color: shadowColor,
              spreadRadius: 3,
              blurRadius: 4,
              offset: const Offset(-2, 1), // changes position of shadow
            ),
          ],
        );
}
