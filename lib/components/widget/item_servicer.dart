import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/tool/hex_colors.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/tool/state.dart';

import 'image_compat.dart';

class ItemServicer extends StatelessWidget {
  const ItemServicer({
    required this.servicer,
    required this.onTap,
    this.pathImage = 'lib/assets/images/',
    this.localImage,
    Key? key,
  }) : super(key: key);

  final Servicer servicer;
  final void Function(Servicer) onTap;
  final String pathImage;
  final String? localImage;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () => onTap(servicer),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              border: Border.all(
                color: HexColor.fromHex('D5D5D5'),
                width: 0.75,
              )),
          child: Card(
            color: colorScheme.background,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ImageCompat(
                localImage != null ? localImage! : servicer.logoUrl,
                pathImages: pathImage,
                fit: BoxFit.contain,
                package: localImage != null ? "sw_loan_connect" : '',
              ),
            ),
          ),
        ),
      );
}
