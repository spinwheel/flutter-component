import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/better_button.dart';
import 'package:sw_core/tool/state.dart';

import 'exit_header.dart';

class ResultScreen extends StatelessWidget {
  const ResultScreen(
      {required bool hasExitHeader,
      required Widget icon,
      required String title,
      required Function() firstButtonPressed,
      required Function() secondButtonPressed,
      required String firstButtonText,
      required String secondButtonText,
      Function()? onExit,
      String? bodyText,
      Key? key})
      : _icon = icon,
        _hasExitHeader = hasExitHeader,
        _onExit = onExit,
        _title = title,
        _firstButtonPressed = firstButtonPressed,
        _secondButtonPressed = secondButtonPressed,
        _firstButtonText = firstButtonText,
        _secondButtonText = secondButtonText,
        _bodyText = bodyText,
        super(key: key);

  final bool _hasExitHeader;
  final Function()? _onExit;
  final Widget _icon;
  final String _title;
  final Function() _firstButtonPressed;
  final Function() _secondButtonPressed;
  final String _firstButtonText;
  final String _secondButtonText;
  final String? _bodyText;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorScheme.background,
      bottomNavigationBar: _bottomButtons(),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Visibility(
                visible: _hasExitHeader,
                child: ExitHeader(
                  onExit: _onExit,
                ),
              ),
              const SizedBox(
                height: 7,
              ),
              _icon,
              const SizedBox(
                height: 30,
              ),
              Text(_title, style: theme.textTheme.headline4),
              const SizedBox(
                height: 24,
              ),
              _bodyText != null
                  ? Text(
                      _bodyText!,
                      style: theme.textTheme.bodyText1,
                    )
                  : const SizedBox()
            ],
          ),
        ),
      ),
    );
  }

  _bottomButtons() => Column(mainAxisSize: MainAxisSize.min, children: [
        BetterButton(
          text: _firstButtonText,
          onPressed: _firstButtonPressed,
        ),
        const SizedBox(
          height: 6,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          alignment: Alignment.center,
          width: double.infinity,
          child: InkWell(
            splashFactory: NoSplash.splashFactory,
            splashColor: Colors.transparent,
            child: Text(_secondButtonText,
                style: TextStyle(
                  color: colorScheme.primary,
                  fontStyle: theme.textTheme.bodyText1!.fontStyle,
                  fontSize: theme.textTheme.bodyText1!.fontSize,
                  fontWeight: theme.textTheme.bodyText1!.fontWeight,
                  fontFamily: theme.textTheme.bodyText1!.fontFamily,
                  height: theme.textTheme.bodyText1!.height,
                  locale: theme.textTheme.bodyText1!.locale,
                  textBaseline: theme.textTheme.bodyText1!.textBaseline,
                )),
            onTap: _secondButtonPressed,
          ),
        ),
        const SizedBox(
          height: 20,
        )
      ]);
}
