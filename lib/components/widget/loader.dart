import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  const Loader({
    Key? key,
    this.width,
    this.height,
  }) : super(key: key);

  final double? width;
  final double? height;

  @override
  Widget build(BuildContext context) => Center(
        child: SizedBox(
          width: width ?? 24,
          height: height ?? 24,
          child: const CircularProgressIndicator.adaptive(),
        ),
      );
}
