import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'box_card.dart';
import 'image_compat.dart';

class ImageCard extends StatefulWidget {
  const ImageCard({
    required this.uri,
    this.title = "",
    this.subTitle = "",
    this.disclaimer = "",
    Key? key,
  }) : super(key: key);

  const ImageCard.pos(
    this.uri, [
    this.title = "",
    this.subTitle = "",
    this.disclaimer = "",
    Key? key,
  ]) : super(key: key);

  final String uri;
  final String title;
  final String subTitle;
  final String disclaimer;

  @override
  _ImageCardState createState() => _ImageCardState();
}

class _ImageCardState extends State<ImageCard> {
  @override
  Widget build(BuildContext context) => BoxCard(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ImageCompat(
              widget.uri,
              height: 241,
              width: 400,
            ),
            const SizedBox(height: 42),
            Text(
              widget.title,
              style: const TextStyle(fontSize: 13),
            ),
            Text(
              widget.subTitle,
              style: const TextStyle(fontSize: 13),
            ),
            const SizedBox(height: 24),
            Text(
              widget.disclaimer,
              style: const TextStyle(fontSize: 14),
            ),
            const SizedBox(height: 24),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: const [
                    ImageCompat('Figure.svg', width: 16, height: 16, fit: BoxFit.fitWidth),
                    SizedBox(width: 8),
                    Text(
                      'Save money with extra payment',
                      style: TextStyle(fontSize: 13),
                    ),
                  ],
                ),
                const SizedBox(height: 18),
                Row(
                  children: const [
                    ImageCompat(
                      'arrows.svg',
                      width: 16,
                      height: 16,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Use round-ups to pay loan faster',
                      style: TextStyle(fontSize: 13),
                    ),
                  ],
                ),
                const SizedBox(height: 18),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    ImageCompat(
                      'Group.svg',
                      height: 16,
                      width: 16,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Pay down the most expensive first',
                      style: TextStyle(fontSize: 13),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 32),
            Container(
              width: 334,
              height: 48,
              color: Theme.of(context).colorScheme.background,
              child: const ElevatedButton(
                  onPressed: null,
                  child: Text(
                    'CONNECT BANK ACCOUNT',
                  )),
            ),
            const SizedBox(height: 8),
            Container(
              width: 334,
              height: 48,
              color: Theme.of(context).colorScheme.background,
              child: const ElevatedButton(
                onPressed: null,
                child: Text('Details & Options'),
              ),
            ),
          ],
        ),
      );
}
