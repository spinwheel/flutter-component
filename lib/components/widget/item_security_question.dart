import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

class ItemFormField extends StatelessWidget {
  ItemFormField({
    Key? key,
    this.label,
    this.icon,
    this.hint,
    this.showHint,
    this.obscureText,
    this.initialValue,
    this.onChanged,
  }) : super(key: key);

  final String? label;
  final String? hint;
  final IconData? icon;
  final bool? showHint;
  final bool? obscureText;
  final String? initialValue;
  final Function(String)? onChanged;
  late final TextFormField field = TextFormField(
    obscureText: obscureText ?? false,
    onChanged: onChanged,
    controller: TextEditingController(),
    decoration: InputDecoration(
      border: theme.inputDecorationTheme.border,
      enabledBorder: theme.inputDecorationTheme.enabledBorder,
      hintText: showHint == true ? hint ?? label : '',
    ),
  );

  @override
  Widget build(BuildContext context) {
    final widget = Padding(
      padding: const EdgeInsets.fromLTRB(32, 0, 32, 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label ?? '',
            style: const TextStyle(fontSize: 14),
            textAlign: TextAlign.start,
          ),
          SizedBox(
            height: 56,
            child: field,
          ),
        ],
      ),
    );

    field.controller?.text = initialValue ?? '';

    return widget;
  }
}

class RawItemFormField extends StatelessWidget {
  RawItemFormField(
      {Key? key,
      this.label,
      this.icon,
      this.hint,
      this.showHint,
      this.enableMargins = true,
      required this.controller,
      this.initialValue})
      : super(key: key);

  final String? label;
  final String? hint;
  final IconData? icon;
  final bool? showHint;
  final bool enableMargins;
  final TextEditingController? controller;
  final String? initialValue;
  late final TextFormField field = TextFormField(
    controller: controller,
    decoration: InputDecoration(
      border: theme.inputDecorationTheme.border,
      enabledBorder: theme.inputDecorationTheme.enabledBorder,
      icon: icon != null ? Icon(icon) : null,
      hintText: showHint == true ? hint ?? label : '',
    ),
    initialValue: initialValue,
  );

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          label != null
              ? Text(
                  label ?? '',
                  style: const TextStyle(fontSize: 14),
                  textAlign: TextAlign.start,
                )
              : Container(),
          Container(
            margin: enableMargins
                ? const EdgeInsets.fromLTRB(32, 16, 32, 0)
                : const EdgeInsets.only(top: 16),
            height: 56,
            child: field,
          )
        ],
      );
}
