import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

class BetterButton extends StatelessWidget {
  const BetterButton({
    required String text,
    required void Function() onPressed,
    bool isLoading = false,
    Key? key,
  })  : _text = text,
        _onPressed = onPressed,
        _isLoading = isLoading,
        super(key: key);

  final void Function() _onPressed;
  final String _text;
  final bool _isLoading;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
        height: 48,
        width: double.infinity,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            splashFactory: NoSplash.splashFactory,
            onPrimary: colorScheme.secondary,
            onSurface: colorScheme.secondary,
          ),
          onPressed: _onPressed,
          child: _isLoading ? _showLoading() : _showText(),
        ));
  }

  _showText() => Text(
        _text,
        style: theme.textTheme.button,
      );

  _showLoading() => CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(colorScheme.onPrimary),
      );
}
