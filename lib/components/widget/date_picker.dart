import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/log.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';

class CupertinoDatePickerParams {
  CupertinoDatePickerParams({
    required this.context,
    required this.onDateTimeChanged,
    required this.maximumYear,
    required this.minimumYear,
    required this.maximumDate,
    required this.minimumDate,
    required this.initialDateTime,
  });

  BuildContext context;
  Function(DateTime) onDateTimeChanged;
  int maximumYear;
  int minimumYear;
  DateTime maximumDate;
  DateTime minimumDate;
  DateTime initialDateTime;

  cupertinoDatePickerBody(Widget datePicker) => SafeArea(
      minimum: const EdgeInsets.only(bottom: 35),
      child: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height * 0.3,
        child: Flex(
          direction: Axis.vertical,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 20,
            ),
            Expanded(
              flex: 2,
              child: datePicker,
            ),
            const SizedBox(
              height: 20,
            ),
            CupertinoButton(
              color: colorScheme.primary,
              onPressed: () => Navigator.of(context, rootNavigator: true).pop("Discard"),
              child: const Text("OK"),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ));
}

class MaterialDatePickerParams {
  MaterialDatePickerParams({
    required this.context,
    required this.initialDate,
    required this.firstDate,
    required this.lastDate,
  });

  BuildContext context;
  DateTime initialDate;
  DateTime firstDate;
  DateTime lastDate;
}

class BlueDatePicker extends StatelessWidget {
  BlueDatePicker({Key? key, String? label, bool? showLabel})
      : _label = label,
        _showLabel = showLabel ?? true,
        super(key: key);

  final bool _showLabel;
  final String? _label;
  final _selectedDate = DateTime.now().obs;
  late final _field = Observer(() => TextFormField(
        decoration: InputDecoration(
          border: theme.inputDecorationTheme.border,
          enabledBorder: theme.inputDecorationTheme.border,
        ),
        enabled: true,
        readOnly: true,
        onTap: () async {
          Platform.isMacOS || Platform.isIOS
              ? await _runPicker()
              : _selectedDate.value = await _runPicker();
          wtf(selectedDate);
        },
        controller: TextEditingController(text: _getFormattedDate()),
      ));

  _getCupertinoParams() => CupertinoDatePickerParams(
      context: buildContext,
      onDateTimeChanged: (dateTime) => _selectedDate.value = dateTime,
      maximumYear: DateTime.now().year,
      minimumYear: 1900,
      maximumDate: DateTime.now(),
      minimumDate: DateTime(1900, 01, 01),
      initialDateTime: _selectedDate.value);

  _getMaterialParams(BuildContext context) => MaterialDatePickerParams(
      context: context,
      initialDate: _selectedDate.value,
      firstDate: DateTime(1900, 01, 01),
      lastDate: DateTime.now());

  Future _runPicker() async => await _getPicker(
      cupertinoParams: _getCupertinoParams(), materialParams: _getMaterialParams(buildContext));

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.fromLTRB(16, 12, 16, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _labelWidget(),
            const SizedBox(height: 6),
            SizedBox(
              height: 56,
              child: _field,
            ),
          ],
        ),
      );

  _labelWidget() => _showLabel
      ? Text(
          _label ?? '',
          style: theme.textTheme.bodyText1,
          textAlign: TextAlign.start,
        )
      : null;

  _getFormattedDate() => formatNumbersUSA(_selectedDate.value.toString());

  String get selectedDate => _getFormattedDate();
}

class DatePicker extends StatelessWidget {
  DatePicker({
    Key? key,
    String? hint,
    bool? showHint,
  })  : _hint = hint ?? '',
        super(key: key);

  final String? _hint;
  final _selectedDate = DateTime.now().obs;
  late final _field = Observer(() => TextFormField(
        decoration: InputDecoration(
          border: theme.inputDecorationTheme.border,
          enabledBorder: theme.inputDecorationTheme.border,
          hintText: _hint,
        ),
        enabled: true,
        readOnly: true,
        onTap: () async {
          Platform.isMacOS || Platform.isIOS
              ? await _runPicker()
              : _selectedDate.value = await _runPicker();
          wtf(selectedDate);
        },
        controller: TextEditingController(text: _getFormattedDate()),
      ));

  _getCupertinoParams() => CupertinoDatePickerParams(
      context: buildContext,
      onDateTimeChanged: (dateTime) => _selectedDate.value = dateTime,
      maximumYear: DateTime.now().year,
      minimumYear: 1900,
      maximumDate: DateTime.now(),
      minimumDate: DateTime(1900, 01, 01),
      initialDateTime: _selectedDate.value);

  _getMaterialParams(BuildContext context) => MaterialDatePickerParams(
      context: context,
      initialDate: _selectedDate.value,
      firstDate: DateTime(1900, 01, 01),
      lastDate: DateTime.now());

  Future _runPicker() async => await _getPicker(
      cupertinoParams: _getCupertinoParams(), materialParams: _getMaterialParams(buildContext));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 16, 32, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 56,
            child: _field,
          ),
        ],
      ),
    );
  }

  _getFormattedDate() => formatNumbersUSA(_selectedDate.value.toString());

  String get selectedDate => _getFormattedDate();
}

Future _getPicker({
  required MaterialDatePickerParams materialParams,
  required CupertinoDatePickerParams cupertinoParams,
}) async =>
    Platform.isMacOS || Platform.isIOS
        ? showCupertinoModalPopup(
            context: cupertinoParams.context,
            builder: (context) => cupertinoParams.cupertinoDatePickerBody(CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                  onDateTimeChanged: (dateTime) => cupertinoParams.onDateTimeChanged(dateTime),
                  maximumYear: cupertinoParams.maximumYear,
                  minimumYear: cupertinoParams.minimumYear,
                  maximumDate: cupertinoParams.maximumDate,
                  minimumDate: cupertinoParams.minimumDate,
                  initialDateTime: cupertinoParams.initialDateTime,
                )))
        : _setDate(
            materialParams.initialDate,
            await showDatePicker(
                context: materialParams.context,
                initialDate: materialParams.initialDate,
                firstDate: materialParams.firstDate,
                lastDate: materialParams.lastDate));

_setDate(DateTime oldValue, DateTime? newValue) {
  if (newValue != null && newValue != oldValue) {
    oldValue = newValue;
  }
  return oldValue;
}
