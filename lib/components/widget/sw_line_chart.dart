import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/tool/ext_state.dart';

class SWLineChart extends StatefulWidget {
  const SWLineChart({
    Key? key,
    required this.y,
    required this.x,
    this.y2,
    this.x2,
    this.lineBarsData,
  }) : super(key: key);

  final double y;
  final double x;
  final double? y2;
  final double? x2;
  final List<LineChartBarData>? lineBarsData;

  @override
  _SWLineChartState createState() => _SWLineChartState();
}

class _SWLineChartState extends State<SWLineChart> {
  @override
  Widget build(BuildContext context) => LineChart(
        LineChartData(
          borderData: FlBorderData(show: false),
          lineBarsData: [
            LineChartBarData(
              colors: [Colors.transparent],
              show: true,
              belowBarData: BarAreaData(
                show: true,
                colors: [
                  colorScheme.primaryVariant,
                  colorScheme.primary,
                ].map((color) => color.withOpacity(0.8)).toList(),
              ),
              spots: [
                FlSpot(0, widget.y),
                FlSpot(widget.x, 0),
              ],
            ),
            if (widget.x2 != null && widget.y2 != null)
              LineChartBarData(
                colors: [Colors.transparent],
                show: true,
                belowBarData: BarAreaData(
                  show: true,
                  colors: [
                    colorScheme.secondaryVariant,
                    colorScheme.secondary,
                  ].map((color) => color.withOpacity(0.8)).toList(),
                ),
                spots: [
                  FlSpot(0, widget.y2!),
                  FlSpot(widget.x2!, 0),
                ],
              ),
            if (widget.lineBarsData != null) ...widget.lineBarsData!,
          ],
          titlesData: FlTitlesData(
            show: true,
            rightTitles: SideTitles(showTitles: false),
            topTitles: SideTitles(showTitles: false),
            leftTitles: SideTitles(
              showTitles: true,
              interval: widget.y / 4,
              getTextStyles: (context, value) => TextStyle(
                color: colorScheme.onBackground,
                fontSize: 13,
              ),
              reservedSize: 32,
            ),
            bottomTitles: SideTitles(
              showTitles: true,
              reservedSize: 22,
              interval: 1,
              getTextStyles: (context, value) => TextStyle(
                color: colorScheme.onBackground,
                fontSize: 13,
              ),
              margin: 8,
            ),
          ),
        ),
        swapAnimationDuration: const Duration(milliseconds: 2000), // Optional
        swapAnimationCurve: Curves.linear, // Optional
      );
}
