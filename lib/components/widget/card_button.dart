import 'package:flutter/material.dart';
import 'package:sw_component/tool/ext_state.dart';

import 'box_card.dart';
import 'card_decor.dart';

class CardButton extends StatefulWidget {
  const CardButton([
    this.title = "",
    this.subTitle = "",
    this.onPressed,
    this.icon = Icons.arrow_forward_ios,
    this.textColor,
    this.backgroundColor,
    Key? key,
  ]) : super(key: key);

  const CardButton.named({
    this.title = "",
    this.subTitle = "",
    this.onPressed,
    this.icon = Icons.arrow_forward_ios,
    this.textColor,
    this.backgroundColor,
    Key? key,
  }) : super(key: key);

  final String title;
  final String subTitle;
  final IconData? icon;
  final void Function()? onPressed;
  final Color? textColor;
  final Color? backgroundColor;

  @override
  State<CardButton> createState() => _CardButtonState();
}

class _CardButtonState extends State<CardButton> {
  @override
  build(BuildContext context) => TextButton(
        onPressed: widget.onPressed,
        child: BoxCard(
          child: _buildContent(),
          padding: const EdgeInsets.all(21),
          decoration: CardDecor(
            backgroundColor: colorScheme.primary,
            shadowColor: shadowColor,
          ),
        ),
      );

  _buildContent() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            flex: 9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.title,
                  style:
                      TextStyle(fontSize: 20, color: widget.textColor ?? colorScheme.onBackground),
                ),
                const SizedBox(height: 16),
                Text(
                  widget.subTitle,
                  style:
                      TextStyle(fontSize: 15, color: widget.textColor ?? colorScheme.onBackground),
                ),
                const SizedBox(height: 8),
              ],
            ),
          ),
          Flexible(
            flex: 1,
            child: Icon(
              widget.icon,
              size: 18,
              color: widget.textColor ?? colorScheme.background,
            ),
          ),
        ],
      );
}
