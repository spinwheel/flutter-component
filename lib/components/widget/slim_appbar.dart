import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/tool/ext_state.dart';
import 'package:sw_core/navigation/nav.dart';

class SlimAppBar extends AppBar {
  SlimAppBar(
    this.text, {
    this.style,
    this.iconButton,
    this.onBackPressed,
    this.centerTitle,
    Color? backgroundColor,
    Key? key,
  }) : super(key: key, backgroundColor: backgroundColor);

  final IconButton? iconButton;
  final String? text;
  final TextStyle? style;
  final VoidCallback? onBackPressed;
  final bool? centerTitle;

  @override
  State<SlimAppBar> createState() => _SlimAppBarState();
}

class _SlimAppBarState extends State<SlimAppBar> {
  @override
  Widget build(BuildContext context) => AppBar(
        centerTitle: widget.centerTitle ?? false,
        backgroundColor: widget.backgroundColor ?? colorScheme.background,
        elevation: 0.0,
        leading: widget.iconButton ??
            IconButton(
              icon: const Icon(
                Icons.arrow_back_ios_new,
                color: Colors.black,
              ),
              iconSize: 16,
              onPressed: () =>
                  widget.onBackPressed != null ? widget.onBackPressed!() : back(context),
            ),
        title: Text(
          widget.text ?? '',
          style: widget.style ?? const TextStyle(fontSize: 16, color: Colors.black),
        ),
      );
}
