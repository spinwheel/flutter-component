import 'package:flutter/material.dart';

class StatusBox extends StatelessWidget {
  const StatusBox(
    this._onTap, {
    double? width,
    Widget? child,
    Color? boxColor,
    Color? borderColor,
    Color? closeButtonColor,
    Color? iconCloseButtonColor,
    Key? key,
  })  : _width = width,
        _child = child,
        _boxColor = boxColor,

        _borderColor = borderColor,
        _closeButtonColor = closeButtonColor,
        _iconCloseButtonColor = iconCloseButtonColor,
        super(key: key);

  final double? _width;
  final void Function() _onTap;
  final Widget? _child;
  final Color? _boxColor;
  final Color? _borderColor;
  final Color? _closeButtonColor;
  final Color? _iconCloseButtonColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: _boxColor ?? const Color(0xffF8D7D9),
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          border: Border.all(color: _borderColor ?? const Color(0xffF5C6Cb))),
      width: _width ?? 250,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(
                18,
              ),
              child: _child ?? const Text(
                "Username or password incorrect. Kindly recheck & try again.",
                style: TextStyle(color: Color(0xff721D24)),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: _onTap,
              child: CloseButton(
                color: _closeButtonColor ?? const Color(0xffDC3545),
                iconColor: _iconCloseButtonColor ?? Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CloseButton extends StatelessWidget {
  const CloseButton(
      {double? width, double? height, double? iconSize, Color? color, Color? iconColor, Key? key})
      : _width = width,
        _height = height,
        _iconSize = iconSize,
        _color = color,
        _iconColor = iconColor,
        super(key: key);

  final double? _width;
  final double? _height;
  final double? _iconSize;
  final Color? _color;
  final Color? _iconColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5, right: 5),
      width: _width ?? 18,
      height: _height ?? 18,
      decoration: BoxDecoration(
          color: _color ?? const Color(0xffDC3545),
          borderRadius: const BorderRadius.all(Radius.circular(360))),
      child: Center(
        child: Icon(
          Icons.close,
          size: _iconSize ?? 18,
          color: _iconColor ?? Colors.white,
        ),
      ),
    );
  }
}
