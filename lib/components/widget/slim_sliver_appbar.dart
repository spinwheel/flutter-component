import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/tool/ext_state.dart';

class SlimSliverAppBar extends StatefulWidget {
  const SlimSliverAppBar(
    this.text, {
    this.style,
    this.iconButton,
    this.onPressed,
    this.floating,
    this.pinned,
    this.backgroundColor,
    Key? key,
  }) : super(key: key);

  final IconButton? iconButton;
  final String? text;
  final TextStyle? style;
  final VoidCallback? onPressed;
  final Color? backgroundColor;
  final bool? floating;
  final bool? pinned;

  @override
  State<SlimSliverAppBar> createState() => _SlimAppBarState();
}

class _SlimAppBarState extends State<SlimSliverAppBar> {
  @override
  Widget build(BuildContext context) => SliverAppBar(
        backgroundColor: widget.backgroundColor ?? colorScheme.background,
        floating: widget.floating ?? true,
        pinned: widget.floating ?? false,
        elevation: 0.0,
        leading: widget.iconButton ??
            IconButton(
              icon: Icon(
                Icons.arrow_back_ios_new,
                color: colorScheme.primary,
              ),
              iconSize: 16,
              onPressed: widget.onPressed ?? pop,
            ),
        title: Text(
          widget.text ?? '',
          style: widget.style ?? TextStyle(fontSize: 16, color: colorScheme.onBackground),
        ),
      );
}
