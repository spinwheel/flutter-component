import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

class BlueItemFormField extends StatelessWidget {
  BlueItemFormField({
    Key? key,
    this.label,
    this.hint,
    this.showHint,
    this.initialValue,
    this.obscureText,
    this.onChanged,
  }) : super(key: key);

  final String? label;
  final String? hint;
  final bool? showHint;
  final bool? obscureText;
  final String? initialValue;
  final Function(String)? onChanged;
  late final TextFormField field = TextFormField(
    decoration: InputDecoration(
      contentPadding: const EdgeInsets.fromLTRB(12.0, 24.0, 12.0, 12.0),
      border: theme.inputDecorationTheme.border,
      enabledBorder: theme.inputDecorationTheme.border,
    ),
    cursorColor: colorScheme.onBackground,
    cursorWidth: 1.0,
    cursorHeight: 16,
    onChanged: onChanged,
    obscureText: obscureText ?? false,
    controller: TextEditingController(),
  );

  @override
  Widget build(BuildContext context) {
    final widget = Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label ?? '',
            style: theme.textTheme.bodyText1,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: label != null ? 6 : 0),
          SizedBox(
            height: 52,
            child: field,
          ),
        ],
      ),
    );

    field.controller?.text = initialValue ?? '';

    return widget;
  }
}

class RawBlueItemFormField extends StatelessWidget {
  RawBlueItemFormField({
    Key? key,
    this.label,
    this.hint,
    this.showHint,
    required this.controller,
    this.initialValue,
    this.obscureText,
    this.onChanged,
  }) : super(key: key);

  final String? label;
  final String? hint;
  final bool? obscureText;
  final Function(String)? onChanged;
  final bool? showHint;
  final TextEditingController? controller;
  final String? initialValue;
  late final Widget field = TextFormField(
    decoration: InputDecoration(
      contentPadding: const EdgeInsets.fromLTRB(12.0, 24.0, 12.0, 12.0),
      border: theme.inputDecorationTheme.border,
      enabledBorder: theme.inputDecorationTheme.border,
    ),
    cursorColor: colorScheme.onBackground,
    cursorWidth: 1.0,
    cursorHeight: 16,
    onChanged: onChanged,
    obscureText: obscureText ?? false,
    controller: controller,
    initialValue: initialValue,
  );

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              label ?? '',
              style: theme.textTheme.bodyText1,
              textAlign: TextAlign.start,
            ),
            SizedBox(height: label != null ? 6 : 0),
            SizedBox(
              height: 52,
              child: field,
            ),
          ],
        ),
      );
}
