import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Reloader extends StatelessWidget {
  const Reloader(
    this.onPressed, {
    Key? key,
    this.width,
    this.height,
    this.text,
  }) : super(key: key);

  final double? width;
  final double? height;
  final String? text;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) => Center(
        child: SizedBox(
          width: width ?? double.infinity,
          height: height ?? double.infinity,
          child: TextButton(
            child: Text(
              text ?? 'Click to try again',
              style: TextStyle(fontSize: 25.0),
              textAlign: TextAlign.center,
            ),
            onPressed: onPressed,
          ),
        ),
      );
}
