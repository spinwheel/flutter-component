import 'package:sw_component/tool/ext_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SliverScroll extends StatefulWidget {
  const SliverScroll({
    required this.children,
    this.title,
    this.style,
    this.iconButton,
    this.onPressed,
    this.backgroundColor,
    Key? key,
  }) : super(key: key);

  final List<Widget> children;
  final IconButton? iconButton;
  final String? title;
  final TextStyle? style;
  final VoidCallback? onPressed;
  final Color? backgroundColor;

  @override
  State<SliverScroll> createState() => _SliverScrollState();
}

class _SliverScrollState extends State<SliverScroll> {
  @override
  Widget build(BuildContext context) => CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: widget.backgroundColor ?? colorScheme.background,
            elevation: 0.0,
            leading: widget.iconButton ??
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios_new,
                    color: colorScheme.primary,
                  ),
                  iconSize: 16,
                  onPressed: widget.onPressed ??
                      () {
                        Navigator.of(context).pop(true);
                      },
                ),
            title: Text(
              widget.title ?? '',
              style: widget.style ?? TextStyle(fontSize: 16, color: colorScheme.onBackground),
            ),
            floating: true,
            pinned: false,
          ),
          SliverList(
            delegate: SliverChildListDelegate(widget.children),
          ),
        ],
      );
}
