import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';

class SWPieChart extends StatelessWidget {
  SWPieChart({required double value, double? chartSize, Key? key})
      : _chartSize = chartSize,
        super(key: key) {
    _value.value = value;
  }

  final _value = 0.0.obs;
  final double? _chartSize;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
         Observer(
            () => Text(
              "${_value.value.toStringAsFixed(0)}%",
            ),
          ),
        SizedBox(
          width: 50,
          height: 50,
          child: Observer(
            () => PieChart(
              PieChartData(
                borderData: FlBorderData(
                  show: false,
                ),
                sectionsSpace: 0,
                centerSpaceRadius: _chartSize ?? MediaQuery.of(context).size.height * 0.025,
                startDegreeOffset: 270,
                sections: [
                  PieChartSectionData(
                    color: Theme.of(context).colorScheme.onSurface,
                    title: "",
                    value: (100 - _value.value).toDouble(),
                    radius: 20,
                  ),
                  PieChartSectionData(
                    color: Theme.of(context).colorScheme.primary,
                    title: "",
                    value: _value.value,
                    radius: 20,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
