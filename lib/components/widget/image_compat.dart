import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sw_core/tool/log.dart';

class ImageCompat extends StatelessWidget {
  const ImageCompat(
    this.uri, {
    this.pathImages = 'lib/assets/images/',
    this.package,
    this.color,
    this.width = double.infinity,
    this.height = double.infinity,
    this.fit = BoxFit.contain,
    Key? key,
  }) : super(key: key);

  final String uri;
  final String pathImages;
  final String? package;
  final Color? color;
  final double width;
  final double height;
  final BoxFit fit;

  @override
  Widget build(BuildContext context) => _onRemoteImage() ?? _onLocalImage();

  Widget? _onRemoteImage() {
    return uri.contains('http')
        ? uri.contains('.svg')
            ? SvgPicture.network(
                uri,
                color: color,
                width: width,
                height: height,
                fit: fit,
              )
            : Image.network(
                uri,
                color: color,
                width: width,
                height: height,
                fit: fit,
              )
        : null;
  }

  Widget _onLocalImage() {
    return uri.contains('.svg')
        ? SvgPicture.asset(
            '$pathImages$uri',
            package: package,
            color: color,
            width: width,
            height: height,
            fit: fit,
          )
        : Image.asset(
            '$pathImages$uri',
            package: package,
            color: color,
            width: width,
            height: height,
            fit: fit,
          );
  }
}
