import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';

class ExtraPay extends StatefulWidget {
  const ExtraPay({Key? key}) : super(key: key);

  @override
  _ExtraPayState createState() => _ExtraPayState();
}

class _ExtraPayState extends State<ExtraPay> {
  @override
  Widget build(BuildContext context) =>
      Container(
        child: _buildDecoration(),
      );


  _buildDecoration() =>
      Expanded(
        child: Container(
          alignment: AlignmentDirectional.topStart,
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            shape: BoxShape.rectangle,
            border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
          ),
          child: _buildContent(),
        ),
      );

  _buildContent() =>
      Column(
        children: [
          const Text('Extra Montly Payment', style: TextStyle(
            fontSize: 14),
          ),
          Row(

          ),
        ],
      );

}
