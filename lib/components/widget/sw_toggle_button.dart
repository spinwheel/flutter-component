import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

class SWToggleButton extends StatelessWidget {
  const SWToggleButton({
    required Function(int index) onPressed,
    required List<bool> isSelected,
    required List<Widget> children,
    Key? key,
  })  : _onPressed = onPressed,
        _isSelected = isSelected,
        _children = children,
        super(key: key);

  final Function(int index) _onPressed;
  final List<bool> _isSelected;
  final List<Widget> _children;

  final bgColor = const Color(0xFFE4E4E4);

  @override
  Widget build(BuildContext context) => Container(
        color: bgColor,
        height: 32,
        child: ToggleButtons(
          isSelected: _isSelected,
          color: Colors.black,
          selectedColor: Colors.white,
          fillColor: colorScheme.primary,
          borderWidth: 1,
          borderRadius: BorderRadius.circular(4),
          children: _children,
          onPressed: _onPressed,
        ),
      );
}
