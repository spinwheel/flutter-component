import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_core/tool/state.dart';

class LogoContainer extends StatelessWidget {
  const LogoContainer({
    String? uri,
    Widget? child,
    String pathImages = 'lib/assets/images/',
    String? package,
    double? width,
    double? height,
    Key? key,
  })  : _uri = uri,
        _child = child,
        _width = width,
        _height = height,
        _package = package,
        _pathImages = pathImages,
        super(key: key);

  final String? _uri;
  final Widget? _child;
  final String _pathImages;
  final String? _package;
  final double? _width;
  final double? _height;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 64,
          height: 64,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(16)),
              color: colorScheme.surface),
        ),
        _child != null
            ? SizedBox(
                width: _width ?? 48,
                height: _height ?? 48,
                child: _child!,
              )
            : ImageCompat(
                _uri!,
                pathImages: _pathImages,
                package: _package ?? 'sw_loan_connect',
                width: _width ?? 48,
                height: _height ?? 48,
                fit: BoxFit.contain,
              )
      ],
    );
  }
}
