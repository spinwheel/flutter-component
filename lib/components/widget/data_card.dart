import 'package:flutter/material.dart';
import 'package:sw_component/tool/ext_state.dart';

import 'box_card.dart';

class DataCard extends StatefulWidget {
  const DataCard({
    String value = "",
    String due = "",
    String rate = "",
    String status = "",
    Function()? onPressed,
    IconData icon = Icons.arrow_forward_ios,
    Color? textColor,
    Color? backgroundColor,
    Decoration? decoration,
    Key? key,
  }) :  this.value = value,
  this.due = due,
  this.rate = rate,
  this.status = status,
  this.onPressed = onPressed,
  this.icon = icon,
  this.textColor = textColor,
  this.backgroundColor = backgroundColor,
  this.decoration = decoration,
        super(key: key);

  const DataCard.named({
    String value = "",
    String due = "",
    String rate = "",
    String status = "",
    Function()? onPressed,
    IconData icon = Icons.arrow_forward_ios,
    Color? textColor,
    Color? backgroundColor,
    Decoration? decoration,
    Key? key,
  }) :  this.value = value,
        this.due = due,
        this.rate = rate,
        this.status = status,
        this.onPressed = onPressed,
        this.icon = icon,
        this.textColor = textColor,
        this.backgroundColor = backgroundColor,
        this.decoration = decoration,
        super(key: key);

  final String value;
  final String due;
  final String rate;
  final String status;
  final Color? textColor;
  final Color? backgroundColor;
  final Decoration? decoration;
  final void Function()? onPressed;
  final IconData? icon;

  @override
  State<DataCard> createState() => _DataCardState();
}

class _DataCardState extends State<DataCard> {
  @override
  build(BuildContext context) => TextButton(
        onPressed: widget.onPressed,
        child: BoxCard(decoration: widget.decoration, width: MediaQuery.of(context).size.width, margin: EdgeInsets.zero, padding: EdgeInsets.all(28), child: _buildContent(), hasShadow: false,),
      );

  _buildContent() => Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
               Text(
                  widget.value,
                  style: TextStyle(
                    fontSize: 18,
                    color: colorScheme.onSurface,
                  ),
                ),
               Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 18,
                    ),
                    const Text(
                      'Due Date:  ',
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      widget.due,
                      style: TextStyle(
                        fontSize: 13,
                        color: colorScheme.onSurface,
                      ),
                    ),
                  ],
                ),
              Flexible(
                flex: 1,
                child: widget.onPressed == null
                    ? const SizedBox(width: 18)
                    : Icon(
                        widget.icon,
                        size: 18,
                      ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Divider(
              color: isDark ? Colors.white30 : Colors.black12,
              height: 32,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Interest Rate',
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      '${widget.rate}%',
                      style: TextStyle(
                        fontSize: 13,
                        color: colorScheme.onSurface,
                      ),
                    ),
                    const SizedBox(width: 64),
                  ],
                ),
              ),
              Flexible(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Status',
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.grey,
                        )),
                    const SizedBox(height: 8),
                    Text(
                      widget.status,
                      style: TextStyle(
                        fontSize: 13,
                        color: colorScheme.onSurface,
                      ),
                    ),
                  ],
                ),
              ),
              const Flexible(
                flex: 2,
                child: SizedBox(
                  width: 80,
                ),
              ),
            ],
          ),
        ],
      );
}
