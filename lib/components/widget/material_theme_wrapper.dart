import 'package:flutter/material.dart';
import 'package:sw_component/service/service_theme.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/state.dart';

class MaterialThemeWrapper extends StatelessWidget {
  const MaterialThemeWrapper({
    Key? key,
    this.lightTheme,
    this.darkTheme,
    required this.child,
  }) : super(key: key);

  final Widget child;
  final ThemeData? lightTheme;
  final ThemeData? darkTheme;

  @override
  Widget build(BuildContext context) => Material(
        color: colorScheme.background,
        child: SafeArea(
          child: Theme(
            data: _getTheme(),
            child: child,
          ),
        ),
      );

  _getTheme() {
    final serviceTheme = getSafe<ServiceTheme>();
    if (isDark) {
      return darkTheme ?? serviceTheme?.darkTheme ?? theme;
    } else {
      return lightTheme ?? serviceTheme?.theme ?? theme;
    }
  }
}
