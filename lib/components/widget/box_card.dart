import 'package:flutter/material.dart';
import 'package:sw_core/tool/state.dart';

import 'card_decor.dart';

class BoxCard extends StatelessWidget {
  const BoxCard({
    Key? key,
    this.height,
    this.width,
    this.hasShadow = true,
    this.padding,
    this.margin,
    this.alignment,
    this.decoration,
    required this.child,
  }) : super(key: key);

  final double? width;
  final double? height;
  final bool hasShadow;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final AlignmentGeometry? alignment;
  final Decoration? decoration;
  final Widget child;

  @override
  Widget build(BuildContext context) => Container(
        width: width,
        height: height,
        alignment: alignment ?? AlignmentDirectional.topStart,
        padding: padding ?? const EdgeInsets.all(24),
        margin: margin ?? const EdgeInsets.all(8),
        decoration: decoration ??
            CardDecor(
                backgroundColor: colorScheme.surface,
                shadowColor: hasShadow ? isDark ? Colors.transparent : Colors.grey.withOpacity(0.2) : Colors.transparent),
        child: child,
      );
}
