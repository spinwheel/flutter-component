import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/reloader.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/tool/log.dart';

import 'loader.dart';

class Fetcher<T> extends StatefulWidget {
  const Fetcher({
    required this.fetch,
    required this.onSuccess,
    this.onLoading,
    this.onError,
    Key? key,
  }) : super(key: key);

  final Future<T> Function() fetch;
  final dynamic Function()? onLoading;
  final Widget Function()? onError;
  final Widget Function(T data) onSuccess;

  @override
  createState() => _FetcherState<T>();
}

class _FetcherState<T> extends State<Fetcher<T>> {
  @override
  build(BuildContext context) => FutureBuilder(
        future: widget.fetch(),
        builder: (context, result) {
          switch (result.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return _caseLoading();
            default:
              return (result.hasError || result.data == null)
                  ? _caseError(result)
                  : _caseSuccess(result);
          }
        },
      );

  _caseLoading() => widget.onLoading != null
      ? widget.onLoading!()
      : const Padding(
          padding: EdgeInsets.all(16.0),
          child: Loader(),
        );

  _caseError(AsyncSnapshot<Object?> result) {
    error(logFormat('error', result.error) +
        logFormat('data', result.data) +
        logFormat('stackTrace', result.stackTrace) +
        logFormat('result', result.toString()));
    return widget.onError != null ? widget.onError!() : Reloader(() => setState(() =>{}));
  }

  _caseSuccess(AsyncSnapshot<Object?> result) => widget.onSuccess(result.data as T);
}
