import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/slider_thumb_image.dart';
import 'package:sw_core/tool/state.dart';

class SWSlider extends StatefulWidget {
  const SWSlider(
    this._value,
    this._maxValue,
    this._minValue,
    this._onChanged,
    this._width,
    this._asset,
    this._label, {
    Key? key,
  }) : super(
          key: key,
        );

  final String _asset;
  final String _label;
  final double _value;
  final double _minValue;
  final double _maxValue;
  final double _width;
  final void Function(double) _onChanged;

  @override
  State<StatefulWidget> createState() => _SWSliderState();
}

class _SWSliderState extends State<SWSlider> {
  @override
  void didChangeDependencies() {
    SliderThumbImageLoader.load(widget._asset)
        .then((value) => setState(() => _customImage = value));
    super.didChangeDependencies();
  }

  ui.Image? _customImage;

  @override
  Widget build(BuildContext context) => _customImage != null
      ? SliderTheme(
          data: SliderThemeData(
            trackHeight: 15,
            thumbShape: SliderThumbImage(_customImage!),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget._label,
              ),
              SizedBox(
                width: widget._width,
                child: Slider(
                  activeColor: colorScheme.primary,
                  value: widget._value,
                  max: widget._maxValue,
                  min: widget._minValue,
                  onChanged: widget._onChanged,
                ),
              ),
            ],
          ),
        )
      : CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(colorScheme.primary));
}
