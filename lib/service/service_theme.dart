import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sw_core/entity_theme/theme_pair.dart';
import 'package:sw_core/interface/service/i_service_theme.dart';
import 'package:sw_core/tool/ext_theme.dart';

class ServiceTheme extends GetxService implements IServiceTheme {
  ServiceTheme(this._lightTheme, this._darkTheme);

  ServiceTheme.fromJson(ThemePair themePair) {
    _lightTheme = ExtTheme.fromJson(themePair.lightTheme);
    _darkTheme = ExtTheme.fromJson(themePair.darkTheme);
  }

  late final ThemeData _lightTheme;
  late final ThemeData _darkTheme;

  @override
  ThemeData get theme => _lightTheme;

  @override
  ThemeData get darkTheme => _darkTheme;

  @override
  ColorScheme get colorSchemeLight => _lightTheme.colorScheme;

  @override
  ColorScheme get colorSchemeDark => _darkTheme.colorScheme;
}
