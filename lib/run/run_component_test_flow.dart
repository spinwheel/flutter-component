import 'package:flutter/material.dart';
import 'package:sw_component/tool/component_test_page.dart';
import 'package:sw_component/tool/sample_theme.dart';
import 'package:sw_core/app/sw_app.dart';

void main() {
  runApp(const ComponentTestApp());
}

class ComponentTestApp extends SWApp {
  const ComponentTestApp({Key? key}) : super(key: key);


  @override
  Widget get home => const ComponentTestPage();

  @override
  String get title => 'Component Test App';

  @override
  bool get debugShowCheckedModeBanner => true;

  @override
  ThemeMode get themeMode => ThemeMode.system;

  @override
  ThemeData get theme => SampleTheme.lightTheme;

  @override
  ThemeData get darkTheme => SampleTheme.darkTheme;
}