import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension ExtState on State {
  ThemeData get theme => Theme.of(context);

  bool get isDark => MediaQuery.of(context).platformBrightness == Brightness.dark;

  Map<String, dynamic> get arguments =>
      ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;

  ColorScheme get colorScheme => Theme.of(context).colorScheme;

  Color get shadowColor => isDark ? Colors.transparent : Colors.grey.withOpacity(0.2);

  pop({bool result = true}) => Navigator.of(context).pop(result);
}
