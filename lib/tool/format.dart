import 'package:intl/intl.dart';

const localeUS = 'en_US';
const dateFormatUTC = 'dd MMM yyyy';
const dateFormatUSA = 'MMM dd yyyy';
const dateFormatNumbersUSA = 'MM-dd-yyyy';

String currency(num? n, [int decimals = 2]) =>
    NumberFormat.simpleCurrency(locale: localeUS, decimalDigits: decimals)
        .format(n?.toDouble() ?? 0);

String formatDate(DateTime date) => DateFormat(dateFormatUTC).format(date);

String formatFirstCharCapital(String string) =>
    string.isNotEmpty ? string.replaceRange(0, 1, string[0].toUpperCase()) : string;

String formatUTC(String? utcTime) {
  return utcTime == null || utcTime.isEmpty
      ? "UNDEFINED"
      : DateFormat(dateFormatUTC).format(utc(utcTime)!);
}

String formatUSA(String? utcTime) {
  return utcTime != null ? DateFormat(dateFormatUSA).format(utc(utcTime)!) : "UNDEFINED";
}

String formatNumbersUSA(String? utcTime) {
  return utcTime != null ? DateFormat(dateFormatNumbersUSA).format(utc(utcTime)!) : "UNDEFINED";
}

DateTime? utc(String? utcTimeString) {
  if (utcTimeString != null) {
    return DateTime.utc(
      _parseSubstring(utcTimeString, 0, 4),
      _parseSubstring(utcTimeString, 5, 7),
      _parseSubstring(utcTimeString, 8, 10),
      _parseSubstring(utcTimeString, 11, 13),
      _parseSubstring(utcTimeString, 14, 16),
      _parseSubstring(utcTimeString, 17, 19),
      _parseSubstring(utcTimeString, 20, 23),
    );
  }
}

_parseSubstring(String? string, int start, int end) {
  if (string != null && string.length >= end) {
    return int.parse(string.substring(start, end));
  }
  return _parseSubstring(string, start, end - 1);
}

logFormat(String header, Object? field) => "$header: $field\n";
