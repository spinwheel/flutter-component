import 'package:flutter/cupertino.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/tool/sample_theme.dart';

class ComponentTestPage extends StatelessWidget {
  const ComponentTestPage({Key? key}) : super(key: key);

  //final themeData = get<SomeThemeObjectFromResponse>();

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        lightTheme: SampleTheme.lightTheme,
        darkTheme: SampleTheme.darkTheme,
        child: _buildBody(),
      );

  _buildBody() => SingleChildScrollView(
        child: Center(
          child: _outerColumn(),
        ),
      );

  _outerColumn() => Column(
        children: [
          _innerColumn(),
        ],
      );

  _innerColumn() => Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          children: [
            const Text('Hello World'),
            ..._anchor(),
            ..._button(),
            ..._card(),
            ..._divider(),
            ..._dotLoader(),
            ..._formControl(),
            ..._header(),
            ..._icons(),
            ..._inputGroup(),
            ..._inputSlider(),
            ..._label(),
            ..._pieChart(),
            ..._progressBar(),
            ..._svgs(),
            ..._text(),
            ..._toggle(),
            ..._tooltip(),
            const Text('This is the end'),
          ],
        ),
      );

  List<Widget> _anchor() => [];
  List<Widget> _button() => [];
  List<Widget> _card() => [];
  List<Widget> _divider() => [];
  List<Widget> _dotLoader() => [];
  List<Widget> _formControl() => [];
  List<Widget> _header() => [];
  List<Widget> _icons() => [];
  List<Widget> _inputGroup() => [];
  List<Widget> _inputSlider() => [];
  List<Widget> _label() => [];
  List<Widget> _pieChart() => [];
  List<Widget> _progressBar() => [];
  List<Widget> _svgs() => [];
  List<Widget> _text() => [];
  List<Widget> _toggle() => [];
  List<Widget> _tooltip() => [];

}
