import 'package:flutter/material.dart';

class SampleTheme {
  static late ThemeData lightTheme = ThemeData(
    fontFamily: 'Inter',
    primarySwatch: Colors.lightBlue,
    colorScheme: light,
    backgroundColor: light.background,
    scaffoldBackgroundColor: light.background,
    cardColor: light.surface,
  );

  static ColorScheme light = ColorScheme(
    //
    brightness: Brightness.light,
    //
    primary: Colors.lightBlue,
    primaryVariant: Colors.blue[900]!,
    onPrimary: Colors.black,
    //
    secondary: Colors.blue[900]!,
    secondaryVariant: Colors.blue[900]!,
    onSecondary: Colors.lightBlue,
    //
    background: Colors.grey[100]!,
    onBackground: Colors.grey[900]!,
    //
    surface: Colors.grey[200]!,
    onSurface: Colors.grey[900]!,
    //
    error: Colors.red[400]!,
    onError: Colors.blue[900]!,
  );

  static late ThemeData darkTheme = ThemeData(
    fontFamily: 'Inter',
    primarySwatch: Colors.teal,
    colorScheme: dark,
    backgroundColor: dark.background,
    scaffoldBackgroundColor: dark.background,
    cardColor: dark.surface,
  );

  static ColorScheme dark = ColorScheme(
    //
    brightness: Brightness.dark,
    //
    primary: Colors.tealAccent,
    primaryVariant: Colors.teal,
    onPrimary: Colors.grey[900]!,
    //
    secondary: Colors.blue[900]!,
    secondaryVariant: Colors.blue[900]!,
    onSecondary: Colors.lightBlue,
    //
    background: Colors.grey[800]!,
    onBackground: Colors.white,
    //
    surface: Colors.blueGrey[700]!,
    onSurface: Colors.white,
    //
    error: Colors.red[400]!,
    onError: Colors.blue[900]!,
  );
}
